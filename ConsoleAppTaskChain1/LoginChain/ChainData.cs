﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace ConsoleAppTaskChain1.LoginChain
{
    public class ChainData<TResult> : IDisposable where TResult : class, new()
    {
        private static ChainData<TResult> loginChainData = null;

        private static object objectLock = new object();

        private TResult loginResult = new TResult();
        private Dictionary<string, Action> actions = new Dictionary<string, Action>();
        private Dictionary<string, Action> continueActions = new Dictionary<string, Action>();
        private Dictionary<string, Action<Exception>> exceptionActions = new Dictionary<string, Action<Exception>>();
        private int timeoutMaxWait;

        public TResult LoginResult
        {
            get
            {
                return LockAndManage<TResult>(() => {
                    return loginResult;
                });
            }

            set
            {
                LockAndManage<TResult>((val) => {
                    loginResult = val;
                }, value);
            }
        }

        public Dictionary<string, Action> Actions
        {
            get
            {
                return LockAndManage<Dictionary<string, Action>>(() => {
                    return actions;
                });
            }

            set
            {
                LockAndManage<Dictionary<string, Action>>((val) => {
                    actions = val;
                }, value);
            }
        }

        public Dictionary<string, Action> ContinueActions
        {
            get
            {
                return LockAndManage<Dictionary<string, Action>>(() => {
                    return continueActions;
                });
            }

            set
            {
                LockAndManage<Dictionary<string, Action>>((val) => {
                    continueActions = val;
                }, value);
            }
        }

        public Dictionary<string, Action<Exception>> ExceptionActions
        {
            get
            {
                return LockAndManage<Dictionary<string, Action<Exception>>>(() => {
                    return exceptionActions;
                });
            }

            set
            {
                LockAndManage<Dictionary<string, Action<Exception>>>((val) => {
                    exceptionActions = val;
                }, value);
            }
        }


        private ChainData()
        {
            Actions = new Dictionary<string, Action>();
            ContinueActions = new Dictionary<string, Action>();
            ExceptionActions = new Dictionary<string, Action<Exception>>();
            timeoutMaxWait = 10;
        }

        public static ChainData<TResult> Instance
        {
            get
            {
                lock(objectLock)
                {
                    if (loginChainData==null)
                    {
                        loginChainData = new ChainData<TResult>();
                    }
                }
                return loginChainData;
            }
        }  

        //Precisamos garantir que "restos" do processo
        //serão limpos após à execução.
        //Por isso tonei essa e a classe abstrata como IDiposable
        //Usando com using acredito que facaremos bem.
        //Mas depois podemos passar um profile.
        public void Dispose()
        {
            Release();
        }

        private void Release()
        {
            Actions = null;
            ContinueActions = null;
            lock (objectLock)
            {
                loginChainData = null;
            }
        }

        //O lock funcionaria bem pois seriam apenas atribuições
        //Mas da maneira que foi estruturado abaixo, evitamos 
        //deadlock entre processos rápidos (o que não é o caso 
        //deste exemplo. Cada processo dura ao menos 10 segundos
        //antes da primeira atribuição.
        private TReturn LockAndManage<TParam, TReturn>(Func<TParam, TReturn> action, TParam val = default(TParam))
        {
            if (Monitor.TryEnter(objectLock, (timeoutMaxWait * 1000)))
            {
                try
                {
                    return action.Invoke(val);
                }
                finally
                {
                    Monitor.Exit(objectLock);
                }
            }
            else
            {
                throw new Exception("Timeout on lock singleton object. Restart process.");
            }
        }
        private TReturn LockAndManage<TReturn>(Func<TReturn> action)
        {
            return LockAndManage<object, TReturn>((val) => {
                return action.Invoke();
            }, null);
        }
        private void LockAndManage<TParam>(Action<TParam> action, TParam val)
        {
            LockAndManage<TParam, object>((val) => {
                action.Invoke(val);
                return val;
            }, val);
        }
    }
}
