﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleAppTaskChain1.LoginChain
{
    public enum ContinueStepEnum
    {
        //Será executada após a tarefa principal
        //Será encadeada com a tarefa principal
        DependentFromParent,
        //Sera executada concorrente à tarefa principal.
        IndependentFromParent
    }

    public abstract class ChainHandler<TResult> : IChainHandler<TResult>, IDisposable where TResult : class, new()
    {
        protected static object lockConsole = new object();

        protected ChainHandler<TResult> nextStep = null;

        public bool Optional { get; private set; }

        private ChainData<TResult> loginChainData = null;

        protected CancellationTokenSource cancellationTokenSource;

        public bool ThrowError { get; set; }
        public Exception LastError { get; set; }

        public TResult ProcessedResult
        {
            get => loginChainData.LoginResult;

            private set => loginChainData.LoginResult = value;
        }

        public TResult SetRequest(TResult loginReq)
        {
            ProcessedResult = loginReq;
            return loginReq;
        }

        public async Task<TResult> GetResult()
        {
            return await Task.Run(() => { return this.ProcessedResult; });
        }


        public ChainHandler<TResult> SetCancelationToken(CancellationTokenSource cancellationToken)
        {
            this.cancellationTokenSource = cancellationToken;
            return this;
        }
        public ChainHandler<TResult> SetNext(ChainHandler<TResult> nextHandler)
        {
            nextStep = nextHandler;
            if (this.cancellationTokenSource != null)
            {
                nextHandler.cancellationTokenSource = this.cancellationTokenSource;
            }

            return nextHandler;
        }

        public ChainHandler<TResult> SetNextOptional(ChainHandler<TResult> nextHandler, CancellationTokenSource cancellationToken = null)
        {
            nextHandler.Optional = true;
            return SetNext(nextHandler);
        }


        public ChainHandler(bool optional = false)
        {
            Optional = optional;
            loginChainData = ChainData<TResult>.Instance;
        }

        protected void SetAction(Action action, bool parentDependent, Action<Exception> actionException = null)
        {
            var idAction = Guid.NewGuid().ToString("N");
            if (parentDependent && loginChainData.Actions.Any())
            {
                loginChainData.ContinueActions.Add(idAction, action);
            }
            else
            {
                loginChainData.Actions.Add(idAction, action);
            }

            if (actionException != null)
            {
                loginChainData.ExceptionActions.Add(idAction, actionException);
            }
        }


        private async Task StartTasks()
        {
            //await Task.Run( () => this.Tasks.ForEach(f => f.Start() ));
            var token = this.cancellationTokenSource.Token;
            Exception taskError = null;

            try
            {
                await CreateAndRun(loginChainData.Actions, loginChainData.ExceptionActions, token, taskError);

                if (taskError != null)
                {
                    throw taskError;
                }
            }
            catch (Exception ex)
            {
                //Logar
                taskError = ex;
            }

            if (taskError == null)
            {
                await CreateAndRun(loginChainData.ContinueActions, loginChainData.ExceptionActions, token, taskError);

                if (taskError != null)
                {
                    throw taskError;
                }

            }
            else
            {
                throw taskError;
            }
        }

        private async Task CreateAndRun(Dictionary<string, Action> actions, Dictionary<string, Action<Exception>> actionExceptions,CancellationToken token, Exception taskError)
        {
            await Task.Run(async () =>
            {
                var taskFactory = new TaskFactory(TaskCreationOptions.AttachedToParent, TaskContinuationOptions.None);
                List<Exception> exList = new List<Exception>();
                List<Task> tasks = new List<Task>();
                foreach (var action in actions)
                {
                    var task = taskFactory.StartNew(() => {
                        try
                        {
                            action.Value.Invoke();
                        }
                        catch (Exception ex)
                        {
                            if (actionExceptions.ContainsKey(action.Key))
                            {
                                actionExceptions[action.Key].Invoke(ex);
                            }
                            exList.Add(ex);
                            this.cancellationTokenSource.Cancel();
                        }
                    }, token);
                    tasks.Add(task);
                }
                await Task.WhenAll(tasks);

                if (exList.Any()) taskError = exList.First();
            });
        }

        protected CancellationToken GetToken()
        {
            CancellationToken token;
            if (this.cancellationTokenSource != null) token = this.cancellationTokenSource.Token;
            return token;
        }

        public abstract Task ExecuteAndRunRequest();

        protected async Task ExecuteAction(Action action, Action<Exception> actionException = null)
        {
            SetAction(action, !this.Optional , actionException);

            if (nextStep != null)
            {
                await nextStep.ExecuteAndRunRequest();
                if (nextStep.nextStep == null)
                {
                    await this.StartTasks();
                }
            }
        }

        public void Dispose()
        {
            loginChainData.Dispose();
        }
    }
}
