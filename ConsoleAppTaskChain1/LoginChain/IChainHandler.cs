﻿using System.Threading.Tasks;

namespace ConsoleAppTaskChain1.LoginChain
{
    public interface IChainHandler<TResult> where TResult: class
    {
        TResult SetRequest(TResult loginReq);

        Task ExecuteAndRunRequest();

        Task<TResult> GetResult();
    }
}
