﻿using ConsoleAppTaskChain1.Model;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleAppTaskChain1.LoginChain
{
    //Cada processo deve implementar ChainHandler 
    //Todos da mesma cadeia ajudarão a setar seus resultados em LoginResult
    public class LoginProfilePinLink : ChainHandler<LoginResult>
    {
        //Podemos ter ou mais uma propriedades específicas para um processo 
        //que tem 1 condição simples de execução. Mas se abusar, pode deixar
        //de ter responsabilidade única.
        public int NumTeste { get; set; }

        public LoginProfilePinLink(bool optional = false) : base(optional)
        {
        }

        public override async Task ExecuteAndRunRequest()
        {
            Console.WriteLine($"Criando Profile {NumTeste.ToString()}");
            //Esse é o token de cancelamento. Usado para 
            //parar o processo em qualquer etapa.
            CancellationToken token = base.GetToken();

            Action action = () =>
            {
                try
                {
                    if (this.ThrowError)
                    {
                        throw new Exception("Você que mandou dar erro!");
                    }

                    int x = 0;
                    int y = 9 + (NumTeste * 2);
                    lock (lockConsole)
                    {
                        Console.SetCursorPosition(x, y);
                        Console.Write($"Carregando Profile {NumTeste.ToString()}");
                    }

                    lock (lockConsole)
                    {
                        Console.SetCursorPosition(x, y+1);
                        Console.Write($"Profile {NumTeste}| ");
                    }

                    x = x + 13;
                    for (int j=1; j<40; j++)
                    {
                        lock (lockConsole)
                        {
                            Console.SetCursorPosition(x, y+1);
                            Console.Write("X");
                        }
                        Random rndX = new Random();
                        int delay = rndX.Next(20, 600);
                        Thread.Sleep(delay);
                        x++;
                    }

                    lock (lockConsole)
                    {
                        Console.SetCursorPosition(x++, y + 1);
                        Console.WriteLine(" |");
                        Console.SetCursorPosition(0, y);
                        Console.WriteLine("Profile executado!             ");
                    }
                    var prof = this.ProcessedResult.Profile;

                    if (string.IsNullOrEmpty(prof)) this.ProcessedResult.Profile = $"Profile carregado! Num:{NumTeste}";
                    else this.ProcessedResult.Profile = prof + "," + NumTeste;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            };
            
            //Aqui colocamos o que vai acontecer quando houver uma exception.
            //Mas atenção: Apenas o que o Profile deve fazer. Responsabilidade única
            //O que deve ser feito se dois ou mais processos falharem, deve 
            //ser feito (nesse exemplo) no Program.Main. Após a execução de TODOS 
            //os processos.
            //Aqui colocamos principalmente logs e coisas de deve ser DESFEITAS na hora
            //de obter o profile.
            Action<Exception> errorAction = (ex) =>
            {
                Console.WriteLine($"Erro! Msg:{ex.Message}");
            };

            await ExecuteAction(action, errorAction);
        }
    }
}
