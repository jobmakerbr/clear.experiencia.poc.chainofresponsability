﻿using ConsoleAppTaskChain1.Model;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleAppTaskChain1.LoginChain
{
    public class LoginTokenPinLink : ChainHandler<LoginResult>
    {
        //Cada processo deve implementar ChainHandler 
        //Todos da mesma cadeia ajudarão a setar seus resultados em LoginResult
        public LoginTokenPinLink(bool optional = false) : base(optional)
        {
        }

        //Neste metodo, cada PinLink executa o trecho de sua responsabilidade
        public override async Task ExecuteAndRunRequest()
        {
            Console.WriteLine("Criando Token");
            //Esse é o token de cancelamento. Usado para 
            //parar o processo em qualquer etapa.
            CancellationToken token = base.GetToken();

            Action action = () =>
            {
                if (this.ThrowError)
                {
                    Console.WriteLine("Vou dar erro!!!");
                    throw new Exception("Você que mandou dar erro!");
                }

                //Aqui vai o código para obter o token
                int x = 0;
                int y = 6;
                lock (lockConsole)
                {
                    Console.SetCursorPosition(x, y);
                    Console.Write("Carregando Token");
                }

                lock (lockConsole)
                {
                    Console.SetCursorPosition(x, y+1);
                    Console.Write($"Token | ");
                }

                x = x + 9;
                for (int j = 1; j < 40; j++)
                {
                    lock (lockConsole)
                    {
                        Console.SetCursorPosition(x, y+1);
                        Console.Write("X");
                    }
                    Random rndX = new Random();
                    int delay = rndX.Next(100, 300);
                    Thread.Sleep(delay);
                    x++;
                }

                lock (lockConsole)
                {
                    Console.SetCursorPosition(x++, y + 1);
                    Console.WriteLine(" |");
                    Console.SetCursorPosition(0, y);
                    Console.WriteLine("Token executado!");
                }

                this.ProcessedResult.Token = $"Token obtido";
            };

            //Executar o seu PinLink e acionar o próximo internamente (definido no processo principal)
            await ExecuteAction(action);
        }
    }
}
