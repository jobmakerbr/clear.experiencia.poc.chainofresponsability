﻿using ConsoleAppTaskChain1.Model;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleAppTaskChain1.LoginChain
{

    public class LoginUserDataPinPink : ChainHandler<LoginResult>
    {
        public LoginUserDataPinPink(bool optional = false) : base(optional)
        {
        }

        public override async Task ExecuteAndRunRequest()
        {
            Console.WriteLine("Criando UserData");

            CancellationToken token = base.GetToken();

            Action action = () =>
            {
                int x = 0;
                int y = 8;
                
                lock (lockConsole)
                {
                    Console.SetCursorPosition(x, y);
                    Console.Write($"Carregando UserData");
                }

                lock (lockConsole)
                {
                    Console.SetCursorPosition(x,y+1);
                    Console.Write($"UserData | ");
                }

                x = x + 12;
                for (int j = 1; j < 40; j++)
                {
                    lock (lockConsole)
                    {
                        Console.SetCursorPosition(x, y+1);
                        Console.Write("X");
                    }
                    Random rndX = new Random();
                    int delay = rndX.Next(50, 200);
                    Thread.Sleep(delay);
                    x++;
                }

                lock (lockConsole)
                {
                    Console.SetCursorPosition(x++, y+1);
                    Console.WriteLine(" |");
                    Console.SetCursorPosition(0, y);
                    Console.WriteLine("UserData executado!");
                }

                this.ProcessedResult.UserData = $"UserData obtido";
            };

            await ExecuteAction(action);
        }
    }
}
