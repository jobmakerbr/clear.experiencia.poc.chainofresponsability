﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleAppTaskChain1.Model
{
    public class LoginResult
    {
        public string Token { get; set; }

        public string UserName { get; set; }

        public string UserData { get; set; }

        public string Profile { get; set; }
    }
}
