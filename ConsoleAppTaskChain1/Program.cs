﻿using ConsoleAppTaskChain1.LoginChain;
using ConsoleAppTaskChain1.Model;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleAppTaskChain1
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                MainLoginAsync().GetAwaiter().GetResult();
            }
            catch (ArgumentException aex)
            {
                Console.WriteLine($"Caught ArgumentException: {aex.Message}");
            }
        }

        private static async Task MainLoginAsync()
        {
            var cts = new CancellationTokenSource();
            //Descomentar para simular um cancelamento forçado
            //Timer timer = new Timer(Elapsed, cts, 5000, Timeout.Infinite);

            //Criar as tarefas (sendo token a principal)
            //Por isso TokenPinkLink fica dentro de um using
            //OBS.: Pinkpink é uma é o nome para cada parte de uma corrente
            using (var loginToken = new LoginTokenPinLink() { ThrowError = false })
            {
                //Indica que a tarefa será executada com o conjunto, mas não depende de outra tarefa,
                //ou seja: faz parte da cadeia de login, mas não é uma tarefa obrigatória.
                var loginUserData = new LoginUserDataPinPink(); // { ThrowError = false };

                //Criar 3 tarefas iguais, mas para teste de erro e identificação.
                var loginProfile1 = new LoginProfilePinLink { NumTeste = 1, ThrowError = false };
                var loginProfile2 = new LoginProfilePinLink { NumTeste = 2, ThrowError = false };
                var loginProfile3 = new LoginProfilePinLink { NumTeste = 3, ThrowError = false };

                //Indicar a ordem que as tarefas devem ser executadas
                //Possibilita incluir/alterar tarefas sem afetar as outras.

                loginToken
                    .SetCancelationToken(cts)
                    .SetNextOptional(loginUserData)
                    .SetNext(loginProfile1)
                    .SetNext(loginProfile2)
                    //Desta maneira fica explicito que UserData 
                    //também faz parte do processo de login, mas sem aguardar o login inteiro terminar
                    .SetNext(loginProfile3);

                //Cria o objeto comum que será usado por TODAS as tarefas
                loginToken.SetRequest(new LoginResult());

                //Executa o processo de login
                await loginToken.ExecuteAndRunRequest();

                //Aguarda o fim do processo e obtém o resultado
                //Todos da mesma cadeia ajudaram a setar seus resultados em LoginResult,
                //portanto aqui obtemos o resultado.

                var request = await loginToken.GetResult();
                Console.SetCursorPosition(0, 18);
                Console.Write("");
                Console.WriteLine("- " + request.Profile);
                Console.WriteLine("- " + request.UserData);
                Console.WriteLine("- " + request.Token);

                Console.WriteLine("Pressione qualquer tecla...");
                Console.ReadKey();
            }
        }
    }
}
