# Projeto

POC com proposta inicial de cadeia de responsabilidade para o processo de login do app
Design Pattern: Chain Of Responsability

## Como executar no seu PC (se quiser)

Faça um clone desse repositório, abra a soluction no visual studio 2019 e pressione F5.
Trata-se de um aplicativo console simples que exemplifica como poderiamos adaptar o processo de login do app.

### Pré-requisitos

Ter o visual studio 2019
Ter o git client (tortoise, etc)

### O que este exemplo faz?

Este exemplo executa 5 processos. Exemplifica 2 processos independentes e outros 3 dependentes do Token.

### Qual o objetivo deste Design Pattern?

Ao receber uma requisição, permitir que cada etapa do processo seja acionada sem criar dependencia entre elas.
Ele pode ser usado para devolver uma resposta única com validações complexas, processos de multiplas responsabilidades, etc. Podendo inclusive, retirar etapas, mover etapas de lugar e alterar uma etapa espcifica do processamento
sem influenciar no que já está funcionando.
Ao final, este design pattern deve retornar um resultado único após ser submetido para cada etapa (PinLink) do processo.
Em termos simples:
1) Você cria cada etapa do processo como uma classe. Para isso, deve-se usar uma estrutura pré-definida que permite definir a próxima etapa da cadeia de processos.
2) Você cria o processo principal informado qual etapa é sucessiva à anterior.
3) Executa e obtém um resultado único com cada etapa do processo tendo feito seu papel.


### Como ficaria o serviço/processo de login

O processo de login terá 3 etapas: 
1) Criar cada o código de cada etapa;
2) Criar o serviço principal;
3) Nesse serviço informar as etapas uma-por-uma, executar e obter o resultado do processo.

Exemplo do processo principal: 

```csharp
	
//Criar as tarefas (sendo token a principal)
//Por isso TokenPinkLink fica dentro de um using
//OBS.: Pinkpink é uma é o nome para cada parte de uma corrente
using (var loginToken = new LoginTokenPinLink() { ThrowError = false })
{
	//Cria o objeto comum de resposta que será preenchido por TODAS as tarefas
	loginToken.SetRequest(new LoginResult());

	//Indica que a tarefa será executada com o conjunto, mas não depende de outra tarefa,
	//ou seja: faz parte da cadeia de login, mas não é uma tarefa obrigatória.
	var loginUserData = new LoginUserDataPinPink(); 

	//Criar 3 tarefas iguais, mas para teste de erro e identificação.
	var loginProfile = new LoginProfilePinLink { NumTeste = 1, ThrowError = false };

	//Indicar a ordem que as tarefas devem ser executadas
	//Possibilita incluir/alterar tarefas sem afetar as outras.
	loginToken
		.SetCancelationToken(cts)
		.SetNextOptional(loginUserData)
		.SetNext(loginProfile);

	//Executa o processo de login
	await loginToken.ExecuteAndRunRequest();

	//Aguarda o fim do processo e obtém o resultado
	var request = await loginToken.GetResult();

	Console.SetCursorPosition(0, 18);
	Console.Write("");
	Console.WriteLine("- " + request.Profile); // <- Preenchido por loginToken
	Console.WriteLine("- " + request.UserData); // <- Preenchido por loginUserData
	Console.WriteLine("- " + request.Token); // <- Preenchido por loginProfile
}


```


### Como um dev fará isso?

Inicialmente, um dev deve criar uma etapa do processo. Esta deve ser uma classe com apenas UMA responsabiliade.
Por exemplo: Obter o token do usuário: 

```csharp

public class LoginTokenPinLink : ChainHandler<LoginResult>
{
	//Cada processo deve implementar ChainHandler 
	//Todos da mesma cadeia ajudarão a setar seus resultados em LoginResult
	public LoginTokenPinLink(bool optional = false) : base(optional)
	{
	}

	//Neste metodo, cada PinLink executa o trecho de sua responsabilidade
	public override async Task ExecuteAndRunRequest()
	{
		Console.WriteLine("Criando Token");
		//Esse é o token de cancelamento. Usado para 
		//parar o processo em qualquer etapa.
		CancellationToken token = base.GetToken();

		Action action = () =>
		{
			if (this.ThrowError)
			{
				Console.WriteLine("Vou dar erro!!!");
				throw new Exception("Você que mandou dar erro!");
			}

			//Aqui vai o código para obter o token
			...
			Acionar HttpClient ou o que seja

			//Preencher o token (reponsabilidade deste PinLink)
			this.ProcessedResult.Token = $"Token obtido";
		};

		//Executar o seu PinLink e acionar o próximo internamente (definido no processo principal)
		//Penso em tornar mais evidente que são duas chamadas
		await ExecuteAction(action);
	}
}

```

## Brincando

Existe a possibilidade de adiocionar uma action de erro para cada PinLink(etapa) da cadeia. 
O único cuidado é que cada PinLink deve ter reponsabilidade única.
Pode-se mover etapas de lugar.
Os PinkLinks marcados como ".SetNextOptional" são executados em concorrencia e sem dependencia de nenhuma outra etapa.

## Contribuir

Deixei permissão para criar PR neste repositório. 
Aceito críticas, sugestões e conselhos... é só uma primeira versão.


## Authors

* **Chain Of Responsibility Design Pattern** - *Artigo legal. O primeiro que vi bastante tempo atrás* - [Codeproject](https://www.c-sharpcorner.com/UploadFile/chinnasrihari/chain-of-responsibility-design-pattern/)
* **Locks mais inteligentes do que com lock()** - *Artigo entigo, mas ainda válido* - [Codeproject](https://www.codeproject.com/Tips/758494/Smarter-than-lock-Cleaner-than-TryEnter)


## License

Quem tiver o link...


